#!/bin/bash

# This script encrypts/decrypts the necessary files

# TODO: check for existence when copying after decryption


repo='git@gitlab.com:thpoons/dot-files.git'
# unencrypted directory of stuff
dplain='dotfiles.plain'
# archived directory of stuff
dar='dotfiles.tar.gz'
# encrypted archive
denc='dotfiles.tar.gz.gpg'

encrypt() {
	# Setup plaintext directory
	# Does it already exist
	if [ -d $dplain ]; then
		# Wanna rm it?
		>&2 echo "Directory $dplain exists"
		while true; do
			echo -n 'Remove it (Y/n) '
			read var
			if [[ "$var" == 'n' ]] || [[ "$var" == 'N' ]]; then
				echo 'Exiting'
				return
			elif [[ "$var" == 'y' ]] || [[ "$var" == 'Y' ]] || [ -z "$var" ]; then
				echo "[-] Deleting $dplain"
				rm -rf $dplain
				break
			else
				echo 'Enter a valid input'
				continue
			fi
		done
	fi
	# Create it
	echo "[+] Creating $dplain"
	mkdir $dplain
	
	# Add the file n stuff
	while read -r path; do
		# Ignore comments
		if [[ "$path" == \#* ]]; then continue; fi
		# Get filename from path
		filename="${path##*/}"
		echo "[+] Copying $filename"
		cp -rpf $HOME/"$path" "$dplain/$filename"
	done < files

	# Archive it
	echo "[*] Creating archive $dar"
	tar -czf "$dar" "$dplain"
	echo "[-] Deleting original"
	rm -rf "$dplain"

	# Begin encryption
	echo "[+] Encrypting archive"
	gpg -o "$denc" --symmetric "$dar"
	echo "[-] Deleting archive"
	rm -rf "$dar"
}

decrypt() {
	# Check for file
	if ! [ -e "$denc" ]; then
		>&2 echo "Encrypted file not found: $denc"
		return
	fi

	# Decrypt
	echo "[+] Decrypting $denc"
	gpg -o "$dar" -d "$denc"
	echo "[-] Deleting encrypted archive"
	rm -rf "$denc"

	# Unarchive
	echo "[+] Unarchiving $dar"
	tar -xf "$dar"
	echo "[-] Deleting archive"
	rm -rf "$dar"

	# cp files to OG locations
#	while read -r path; do
#		# Ignore comments
#		if [[ "$path" == \#* ]]; then continue; fi
#		# Get filename from path
#		filename=$(basename "$path")
#		realpath=$(dirname "$path")
#		echo "[+] Copying $filename"
#		# incase it doesn't exist
#		mkdir -p "$HOME/$realpath"
#		# cp it
#		cp -rpf "$dplain/$filename" "$HOME/$path"
#	done < files
#
#	echo "[-] Removing original directory"
#	rm -rf "$dplain"
	echo 'NOT COPYING...'
}

main() {
	case $1 in
		e|encrypt)
			echo "Encrypting..."
			encrypt
			;;
		d|decrypt)
			echo "Decrypting..."
			decrypt
			;;
		*)
			echo "Improper input..."
			;;
	esac
}
main $@
